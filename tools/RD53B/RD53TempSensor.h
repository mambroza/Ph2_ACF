#ifndef RD53TempSensor_H
#define RD53TempSensor_H

#include "RD53BTool.h"

namespace RD53BTools {

template <class Flavor>
struct RD53TempSensor : public RD53BTool<RD53TempSensor, Flavor> {
    using Base = RD53BTool<RD53TempSensor, Flavor>;
    using Base::Base;
	
    struct ChipResults {
		double idealityFactor[4];
		double valueLow;
		double valueHigh;
		double calibDV[4][2];
		double calibNTCtemp[4][2];
		double calibSenstemp[4][2];
		double NTCvoltage;
		//double ADCslope;
		//double ADCintercept;
		//double currentFactor;
    };

	
	static constexpr double power[2] = {1.25, 1.92};
	static constexpr int sensor_VMUX[4] = {0b1000000000101, 0b1000000000110, 0b1000000001110, 0b1000000010000};
	

    ChipDataMap<ChipResults> run();

    void draw(const ChipDataMap<ChipResults>& results) const;
};

}

#endif


