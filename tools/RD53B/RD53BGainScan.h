#ifndef RD53BGainScan_H
#define RD53BGainScan_H

#include "RD53BInjectionTool.h"

#include "../Utils/xtensor/xoptional.hpp"

namespace RD53BTools {

template <class>
struct RD53BGainScan; // forward declaration

template <class Flavor>
const auto ToolParameters<RD53BGainScan<Flavor>> = make_named_tuple(
    std::make_pair("injectionTool"_s, RD53BInjectionTool<Flavor>()),
    std::make_pair("vcalMed"_s, 300u),
    std::make_pair("vcalRange"_s, std::vector<size_t>({200, 8000})),
    std::make_pair("vcalStep"_s, 20u)
);

template <class Flavor>
struct RD53BGainScan : public RD53BTool<RD53BGainScan, Flavor> {
    using Base = RD53BTool<RD53BGainScan, Flavor>;
    using Base::Base;
    using Base::param;

    struct ChipResult {
        xt::xtensor<double, 2> slope;
        xt::xtensor<double, 2> intercept;
        xt::xtensor<double, 2> rSquared;
        xt::xtensor<double, 2> Scan;
    };
    
    void init();

    ChipDataMap<ChipResult> run(Task progress) const;

    void draw(const ChipDataMap<ChipResult>& occMap);

private:
    const auto& offset(size_t i) const { return param("injectionTool"_s).param("offset"_s)[i]; }
    const auto& size(size_t i) const { return param("injectionTool"_s).param("size"_s)[i]; }
    auto rowRange() const { return xt::xrange<std::ptrdiff_t>(offset(0), offset(0) + size(0)); }
    auto colRange() const { return xt::xrange<std::ptrdiff_t>(offset(1), offset(1) + size(1)); }

    xt::xtensor<size_t, 1> vcalBins;
};

}

#endif