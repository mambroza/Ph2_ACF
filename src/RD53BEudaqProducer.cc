#ifndef __EUDAQ__

#include <iostream>

int main() {
    std::cout << "RD53BEudaqProducer has not been compiled (set CompileWithEUDAQ to true in setup.sh)" << std::endl;
}

#else
#include "../System/SystemController.h"
#include "../HWInterface/RD53BInterface.h"
#include "../Utils/RD53BUtils.h"


#include <TFile.h>
#include <TH1I.h>
#include <TH2I.h>

#pragma GCC diagnostic push 
#pragma GCC diagnostic ignored "-Wunused-function"
#include "eudaq/Factory.hh"
#include "eudaq/Producer.hh"
#include "../user/CMSIT/module/include/CMSITEventData.hh"
#pragma GCC diagnostic pop

#include "../Utils/argvparser.h"

#include "boost/archive/binary_oarchive.hpp"
#include "boost/serialization/vector.hpp"

#include <chrono>
#include <thread>


INITIALIZE_EASYLOGGINGPP

using namespace Ph2_System;
using namespace Ph2_HwDescription;
using namespace Ph2_HwInterface;
using namespace RD53BUtils;


template <class T>
using ChipDataMap = std::map<ChipLocation, T>;

struct RD53BEudaqProducer : public eudaq::Producer {
    using eudaq::Producer::Producer;

    static const uint32_t m_id_factory = eudaq::cstr2hash("RD53BEudaqProducer");
    
    void DoInitialise() override {
        std::cout << "DoInitialise" << std::endl;
        auto conf = GetInitConfiguration();
        configFile = conf->Get("configFile", "CROC.xml");
        sensorType = conf->Get("sensorType", "50x50");
        triggerDuration = conf->Get("triggerDuration", 1);
        handshakeMode = conf->Get("handshakeMode", true);
        fiftyOhmEnable = conf->Get("fiftyOhmEnable", true);
        decoderThreads = conf->Get("decoderThreads", 4);
    }
    
    void DoConfigure() override {
        std::cout << "DoConfigure" << std::endl;
        auto conf = GetConfiguration();
        conf->Print();
        if (system)
            system->Destroy();
        system = std::make_unique<SystemController>();
        system->InitializeHw(configFile);
        system->InitializeSettings(configFile);
        system->ConfigureHw();
	    std::cout << "Configure done" << std::endl;
    }
    
    void DoStartRun() override {
        std::cout << "DoStartRun" << std::endl;
        done = false;
        if (system->fDetectorContainer->at(0)->getFrontEndType() == FrontEndType::RD53B)
            DAQThread.reset(new std::thread(&RD53BEudaqProducer::DAQLoop<RD53BFlavor::ATLAS>, std::ref(*this)));
        else
            DAQThread.reset(new std::thread(&RD53BEudaqProducer::DAQLoop<RD53BFlavor::CMS>, std::ref(*this)));
    }
    
    void DoStopRun() override {
        std::cout << "DoStopRun" << std::endl;
        done = true;
        DAQThread->join();
    }
    
    void DoReset() override {
        std::cout << "DoReset" << std::endl;
        if (system)
            system->Destroy();
        system.reset();
    }
    
    void DoTerminate() override {
        std::cout << "DoTerminate" << std::endl;
        {
            std::lock_guard<std::mutex> lk(terminateMutex);
            terminate = true;
            std::cerr << "Notifying...\n";
        }
        terminateCV.notify_all();
    }

    void DoStatus() override {
        SetStatusTag("triggerCount", std::to_string(triggerCount));
        SetStatusTag("eudaqEventCount", std::to_string(eudaqEventCount));
        SetStatusTag("fwEventCount", std::to_string(fwEventCount));
        SetStatusTag("wordsRead", std::to_string(wordsRead));
    }

    template <class It>
    auto sendEvents(It begin, It end) {
        std::cout << "Event ID: " << eudaqEventCount << ", TLU ID: " << begin->tlu_trigger_id << std::endl;

        auto eudaqEvent = eudaq::Event::MakeUnique("CMSIT");
        eudaqEvent->SetTriggerN(begin->tlu_trigger_id);
        eudaqEventCount++;

        fwEventCount += end - begin;

        CMSITEventData::EventData serializableEvent{
            std::time(nullptr),
            (uint32_t)triggerDuration, 
            begin->l1a_counter,
            begin->tdc,
            begin->BCID,
            begin->tlu_trigger_id, 
            {}
        };

        for (auto it = begin; it != end; ++it) {
            for (const auto& event : it->events) {
	            RD53Base* current_chip = nullptr;
                for_each_device<Chip>(*system, [&] (Chip* chip) { 
                    auto rd53 = static_cast<RD53Base*>(chip);
                    if (rd53->getHybridId() == event.hybridId && rd53->getChipLane() == event.chipLane)
                        current_chip = rd53;
                });

                if (current_chip != nullptr) {

                    std::vector<CMSITEventData::HitData> hits;
                    hits.reserve(event.hits.size());
                    std::transform(event.hits.begin(), event.hits.end(), std::back_inserter(hits), [] (const auto& hit){
                        return CMSITEventData::HitData{hit.row, hit.col, hit.tot};
                    });

                    for (const auto& hit : event.hits) {
                        ++hitCountMap[current_chip](hit.row, hit.col);
                        ++triggerIdCount[current_chip][it->l1a_counter % triggerDuration];
                    }
                    serializableEvent.chipData.push_back({
                        current_chip->comment(),
                        current_chip->getId(),
                        current_chip->getChipLane(),
                        current_chip->getHybridId(),
                        event.triggerId,
                        event.triggerId,
                        it->BCID,
                        std::move(hits)
                    });
                }
                else {
		  EUDAQ_WARN("chip not found");
		}
            }
        }
        
        std::ostringstream os;
        boost::archive::binary_oarchive ar(os);
        ar << serializableEvent;

        eudaqEvent->AddBlock(0, os.str().c_str(), os.str().size());
        
        this->SendEvent(std::move(eudaqEvent));
    }

    void sendEvents(bool sendAll=false) {

        size_t tlu_trigger_id = eventContainers.front().tlu_trigger_id;
        auto sentEventsEnd = eventContainers.begin();

        for (auto it = eventContainers.begin() + 1; it != eventContainers.end(); ++it) {
            if (it->tlu_trigger_id != tlu_trigger_id) {
                sendEvents(sentEventsEnd, it);
                sentEventsEnd = it;
                tlu_trigger_id = it->tlu_trigger_id;
            }
        }
        if (sendAll && sentEventsEnd != eventContainers.end()) {
            sendEvents(sentEventsEnd, eventContainers.end());
            eventContainers.clear();
        }
        else 
            eventContainers.erase(eventContainers.begin(), sentEventsEnd);
    }
    
    template <class Flavor>
    void decodeData(std::vector<uint32_t>& data) {
        try {
            size_t n_bits = RD53BEventDecoding::decode_events<Flavor>(data, eventContainers);
            data.erase(data.begin(), data.begin() + n_bits / 32);
        }
        catch (std::runtime_error&) {
            return;
        }
    }

    template <class Flavor>
    void DAQLoop() {
        std::cout << "Entering DAQ Loop" << std::endl;

        eudaqEventCount = 0;
        fwEventCount = 0;

        hitCountMap.clear();
        triggerIdCount.clear();

        for_each_device<Chip>(*system, [&] (Chip* chip) {
            hitCountMap[chip] = xt::zeros<double>({Flavor::nRows, Flavor::nCols});
            triggerIdCount[chip].resize(triggerDuration);
        });

        auto& fwInterface = *static_cast<RD53FWInterface*>(system->fBeBoardFWMap.at(0));

        auto fastCmdCfg = fwInterface.getLocalCfgFastCmd();

        fastCmdCfg->n_triggers = 0;
        fastCmdCfg->trigger_duration = triggerDuration - 1;
        fastCmdCfg->trigger_source = RD53FWInterface::TriggerSource::TLU;
        fastCmdCfg->backpressure_en = false;

        fwInterface.ConfigureFastCommands(fastCmdCfg);

        RD53FWInterface::DIO5Config dio5Cfg;

        dio5Cfg.enable = true;
        dio5Cfg.ext_clk_en = false;
        dio5Cfg.ch_out_en = 5;
        dio5Cfg.ch1_thr = 40;
        dio5Cfg.ch2_thr = 40;
        dio5Cfg.ch3_thr = 40;
        dio5Cfg.ch4_thr = 40;
        dio5Cfg.ch5_thr = 40;
        dio5Cfg.tlu_en = true;
        dio5Cfg.tlu_handshake_mode = handshakeMode ? 2 : 0;
        dio5Cfg.fiftyohm_en = 0x1D | (fiftyOhmEnable << 1);
        fwInterface.ConfigureDIO5(&dio5Cfg);


        std::vector<std::string> regs = {
            "user.ctrl_regs.fast_cmd_reg_2.trigger_source",
            "user.ctrl_regs.fast_cmd_reg_2.backpressure_en",
            "user.ctrl_regs.fast_cmd_reg_2.init_ecr_en",
            // "user.ctrl_regs.fast_cmd_reg_2.veto_en",
            "user.ctrl_regs.fast_cmd_reg_2.ext_trig_delay",
            "user.ctrl_regs.fast_cmd_reg_2.trigger_duration",
            // "user.ctrl_regs.fast_cmd_reg_2.HitOr_enable_l12",
            "user.ctrl_regs.fast_cmd_reg_3.triggers_to_accept",
            "user.ctrl_regs.ext_tlu_reg1.dio5_en",
            "user.ctrl_regs.ext_tlu_reg1.dio5_ch_out_en",
            "user.ctrl_regs.ext_tlu_reg1.dio5_term_50ohm_en",
            "user.ctrl_regs.ext_tlu_reg1.dio5_ch1_thr",
            "user.ctrl_regs.ext_tlu_reg1.dio5_ch2_thr",
            "user.ctrl_regs.ext_tlu_reg2.dio5_ch3_thr",
            "user.ctrl_regs.ext_tlu_reg2.dio5_ch4_thr",
            "user.ctrl_regs.ext_tlu_reg2.dio5_ch5_thr",
            "user.ctrl_regs.ext_tlu_reg2.tlu_en",
            "user.ctrl_regs.ext_tlu_reg2.tlu_handshake_mode",
            "user.ctrl_regs.ext_tlu_reg2.tlu_delay",
            "user.ctrl_regs.reset_reg.ext_clk_en"
        };

        for (const auto& reg : regs)
            std::cout << reg << ": " << fwInterface.ReadReg(reg) << std::endl;

        std::vector<uint32_t> data;
        wordsRead = 0;

        fwInterface.Start();

	    size_t wordsToRead;

        static const size_t memorySize = (1ul << 32) / 32;

        while ((wordsToRead = fwInterface.ReadReg("user.stat_regs.words_to_read")) > 0 || !done)  {
            auto t0 = std::chrono::steady_clock::now();

            triggerCount = fwInterface.ReadReg("user.stat_regs.trigger_cntr");
            
            if (wordsToRead > 0) {
                wordsToRead = std::min(wordsToRead, memorySize - wordsRead);
                auto chunk = fwInterface.ReadBlockRegOffset("ddr3.fc7_daq_ddr3", wordsToRead, wordsRead % memorySize);
                data.insert(data.end(), chunk.begin(), chunk.end());
                wordsRead += wordsToRead;
                
                decodeData<Flavor>(data);
                sendEvents();
            }

            std::this_thread::sleep_until(t0 + std::chrono::milliseconds(25));
        }

        fwInterface.Stop();

        if (data.size())
            decodeData<Flavor>(data);

        sendEvents();

        std::string fname = "RD53BEudaqProducer_" + std::to_string(GetRunNumber()) + ".root";

        TFile f(fname.c_str(), "CREATE");

        for_each_device<Chip>(*system, [&] (Chip* chip) {
            TH2I* hitHist = new TH2I("HitHist", "Hit Count Map", Flavor::nCols, 0, Flavor::nCols, Flavor::nRows, 0, Flavor::nRows);
            
            for (size_t i = 0; i < Flavor::nRows; ++i)
                for (size_t j = 0; j < Flavor::nCols; ++j)
                    hitHist->Fill(j, Flavor::nRows - i - 1, hitCountMap[chip](i, j));

            hitHist->Write();

            TH1I* triggerIdHist = new TH1I("TriggerId", "TriggerId histogram", triggerDuration, 0, triggerDuration);
            
            for (size_t i = 0; i < triggerDuration; ++i) {
                triggerIdHist->Fill(i, triggerIdCount[chip][i]);
            }

            triggerIdHist->Write();
        });
            
        std::cout << "Leaving DAQ Loop" << std::endl;
    }

    void Join() {
        std::unique_lock<std::mutex> lk(terminateMutex);
        std::cerr << "Waiting... \n";
        terminateCV.wait(lk, [&]{return terminate;});
        std::cerr << "...finished waiting.\n";
    }

private:
    std::condition_variable terminateCV;
    std::mutex terminateMutex; 
    bool terminate = false;

    std::string configFile;
    std::string sensorType;
    size_t triggerDuration;
    bool handshakeMode;
    bool fiftyOhmEnable;
    size_t decoderThreads;
    
    std::atomic<size_t> fwEventCount;
    std::atomic<size_t> eudaqEventCount;
    std::atomic<size_t> triggerCount;
    std::atomic<size_t> wordsRead;

    ChipDataMap<xt::xtensor<size_t, 2>> hitCountMap;
    ChipDataMap<std::vector<size_t>> triggerIdCount; 

    std::vector<RD53BEventDecoding::RD53BEventContainer> eventContainers;
    
    std::unique_ptr<SystemController> system;
    std::unique_ptr<std::thread> DAQThread;
    std::atomic<bool> done;
};

namespace{
    auto dummy0 = eudaq::Factory<eudaq::Producer>::Register<RD53BEudaqProducer, const std::string&, const std::string&>(RD53BEudaqProducer::m_id_factory);
}

int main(int argc, char** argv) {
    std::cout << "hello" << std::endl;
    CommandLineProcessing::ArgvParser cmd;
    
    cmd.defineOption("eudaqAddress", "EUDAQ-IT run control address (e.g. tcp://localhost:44000)", CommandLineProcessing::ArgvParser::OptionRequiresValue);
    
    int result = cmd.parse(argc, argv);
    if(result != CommandLineProcessing::ArgvParser::NoParserError)
    {
        LOG(INFO) << cmd.parseErrorDescription(result);
        exit(EXIT_FAILURE);
    }

    std::string eudaqAddress = cmd.foundOption("eudaqAddress") == true ? cmd.optionValue("eudaqAddress") : "tcp://localhost:44000";

    std::cout << "making producer" << std::endl;
    auto producer = std::static_pointer_cast<RD53BEudaqProducer>(eudaq::Producer::Make("RD53BEudaqProducer", "RD53BEudaqProducer", eudaqAddress));
    producer->Connect();
    std::cout << "connected" << std::endl;
    producer->Join();
    return 0;
}

#endif
