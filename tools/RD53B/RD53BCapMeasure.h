#ifndef RD53BCapMeasure_H
#define RD53BCapMeasure_H

#include "RD53BTool.h"

//#include "../ProductionTools/ITchipTestingInterface.h"

namespace RD53BTools {

template <class Flavor>
struct RD53BCapMeasure : public RD53BTool<RD53BCapMeasure, Flavor> {
    using Base = RD53BTool<RD53BCapMeasure, Flavor>;
    using Base::Base;
	
	
    struct CapVoltages {
        double CapVolts[4];
    };


    using capVoltages = ChipDataMap<RD53BCapMeasure::CapVoltages>;
    capVoltages run() const;

    void draw(const capVoltages& results) const;

};

}

#endif


