#include "RD53MuxScan.h"

#include "../ProductionTools/ITchipTestingInterface.h"

#include "../DQMUtils/RD53MuxScanHistograms.h"

#include <iostream>

namespace RD53BTools {

template <class Flavor>
typename RD53MuxScan<Flavor>::Results RD53MuxScan<Flavor>::run() const {
    Results results;
    auto& chipInterface = Base::chipInterface();

    Ph2_ITchipTesting::ITpowerSupplyChannelInterface dKeithley2410(Base::system().fPowerSupplyClient, "TestKeithley", "Front");

    std::cout << "setupKeithley2410ChannelSense..." << std::endl;

    dKeithley2410.setupKeithley2410ChannelSense(VOLTAGESENSE, 2.0);

    std::cout << "setupKeithley2410ChannelSense done!" << std::endl;

    auto chip = Base::firstChip();
	
    
    chipInterface.WriteReg(chip, "IMonitor", Flavor::IMuxMap.at("HIGH_Z"));
	for (const auto& VMuxVar : Flavor::VMuxMap) {

	    
		if (VMuxVar.first == "HIGH_Z" || VMuxVar.first == "IMUX_OUT") 
			continue;

        chipInterface.WriteReg(chip, "MonitorEnable", 1); //Choose MUX entry
        chipInterface.WriteReg(chip, "VMonitor", VMuxVar.second);
        // usleep(1000000);
        double external = dKeithley2410.getVoltage();
        chipInterface.SendGlobalPulse(chip, {"ADCStartOfConversion"}); //ADC start conversion
        double adc = chipInterface.ReadReg(chip, "MonitoringDataADC");
        results.VMuxVolt.insert({VMuxVar.first, {external, adc}});
        LOG(INFO) << BOLDBLUE << "VMUX: " << BOLDYELLOW <<  VMuxVar.first << " (" << +VMuxVar.second << ")" << RESET << " external: " << external << " adc: " << adc;
    }


    chipInterface.WriteReg(chip, "VMonitor", Flavor::VMuxMap.at("IMUX_OUT"));
    for (const auto& IMuxVar : Flavor::IMuxMap) {
        if (IMuxVar.first == "HIGH_Z") 
            continue;
        chipInterface.WriteReg(chip, "MonitorEnable", 1); //Choose MUX entry
        chipInterface.WriteReg(chip, "IMonitor", IMuxVar.second);
        // usleep(1000000);
        double external = dKeithley2410.getVoltage();
        chipInterface.SendGlobalPulse(chip, {"ADCStartOfConversion"}); //ADC start conversion
        double adc = chipInterface.ReadReg(chip, "MonitoringDataADC");
        results.IMuxVolt.insert({IMuxVar.first, {external, adc}});
        LOG(INFO) << BOLDBLUE << "IMUX: " << BOLDYELLOW << IMuxVar.first << " (" << +IMuxVar.second << ")" << RESET << " external: " << external << " adc: " << adc;
    }

    return results;
}

template <class Flavor>
void RD53MuxScan<Flavor>::draw(const Results& results) const {
    std::ofstream outFile(Base::getOutputFilePath("muxScan.csv"));

	outFile << "type, var, external, adc\n";

    for (const auto item : results.VMuxVolt)
		outFile << "VMux, " << item.first << ", " << item.second.first << ", " << item.second.second << '\n'; 

	for (const auto item : results.IMuxVolt)
		outFile << "IMux, " << item.first << ", " << item.second.first << ", " << item.second.second << '\n'; 
    
}

template class RD53MuxScan<RD53BFlavor::ATLAS>;
template class RD53MuxScan<RD53BFlavor::CMS>;

}


