
#include "RD53IVScan.h"

#include "../ProductionTools/ITchipTestingInterface.h"

#include <iostream>

namespace RD53BTools
{

template <class Flavor>
ChipDataMap<typename RD53IVScan<Flavor>::ChipResults> RD53IVScan<Flavor>::run(Task progress) const {
    // TODO The following line should be eliminated somehow - Antonio Nov 24 2021 18:41
        ChipDataMap<ChipResults>                                      results;
        auto&                                                         chipInterface = *static_cast<RD53BInterface<Flavor>*>(Base::system().fReadoutChipInterface);
        std::vector<Ph2_ITchipTesting::ITpowerSupplyChannelInterface> channelsPS;

        //LOG(INFO) << "PowerSupply client" << Base::system().fPowerSupplyClient << RESET;

        unsigned int const vTrimD          = Base::param("VDDDTrim"_s);
        unsigned int const vTrimA          = Base::param("VDDATrim"_s);
        bool               trimVDDOnly     = Base::param("trimVDDOnly"_s);
        
        for_each_device<Chip>(Base::system(), [&](Chip* chip) {
            chipInterface.WriteReg(chip, "TRIM_VREFD",vTrimD);
            chipInterface.WriteReg(chip, "TRIM_VREFA",vTrimA);
        });

        if (trimVDDOnly) return results;

        Ph2_ITchipTesting::ITIVSLDOTestInterface itIVtest(Base::system().fPowerSupplyClient, Base::param("configFile"_s));

        LOG(INFO) << "[RD53IVScan] configFile = " << Base::param("configFile"_s) << RESET;

        if(Base::param("type"_s) == "complete")
            itIVtest.runITIVSLDOScan();
        else if(Base::param("type"_s) == "steps")
        {
            float vStart1         = Base::param("scanPointCurrentRangeFirst"_s)[0];
            float vStop1          = Base::param("scanPointCurrentRangeFirst"_s)[1];
            if (vStop1 > vStart1)
            {
                LOG(ERROR) << BOLDRED << "Invalid first current range: the first curve should go from an high value to a low value " << RESET;
                return results;
            }
            
            float vStart2        = Base::param("scanPointCurrentRangeSecond"_s)[0];
            float vStop2         = Base::param("scanPointCurrentRangeSecond"_s)[1];
            if (vStop2 < vStart2)
            {
                LOG(ERROR) << BOLDRED << "Invalid second current range: the second curve should go from a low value to a high value " << RESET;
                return results;
            }
            
            float vStep        = Base::param("scanPointCurrentStep"_s);
            float finalCurrent = Base::param("finalCurrentPoint"_s);
            float CurrentTmp   = vStart1;
            
            std::vector<float> FirstIVCurve;
            std::vector<float> SecondIVCurve;

            while ( CurrentTmp >= (vStop1 - 1e-5) )
            {
                FirstIVCurve.push_back(CurrentTmp);
                CurrentTmp -= vStep;
            }
            CurrentTmp=vStart2;
            while ( CurrentTmp <= (vStop2 + 1e-5) )
            {
                SecondIVCurve.push_back(CurrentTmp);
                CurrentTmp += vStep;
            }

            Ph2_ITchipTesting::ITScannerCardInterface        scannerCardKeithley(Base::system().fPowerSupplyClient, Base::param("configFile"_s), Base::param("multimeterName"_s));
            Ph2_ITchipTesting::ITpowerSupplyChannelInterface digitalChannel(Base::system().fPowerSupplyClient, Base::param("powerSupplyName"_s), Base::param("channelID_Digital"_s));
            Ph2_ITchipTesting::ITpowerSupplyChannelInterface analogChannel(Base::system().fPowerSupplyClient, Base::param("powerSupplyName"_s), Base::param("channelID_Analog"_s));
            channelsPS.push_back(digitalChannel); // Keep digital as first and analog as second
            channelsPS.push_back(analogChannel);  // Keep digital as first and analog as second
            scannerCardKeithley.prepareMultimeter();
            float voltageProtection = Base::param("powerSupplyVoltageProtection"_s);
            float voltageCompliance = Base::param("powerSupplyVoltageCompliance"_s);
            for(unsigned int v = 0; v < channelsPS.size(); ++v) channelsPS[v].setVoltageProtection(voltageProtection);
            for(unsigned int v = 0; v < channelsPS.size(); ++v) channelsPS[v].setVoltageCompliance(voltageCompliance);
            for(unsigned int v = 0; v < channelsPS.size(); ++v) channelsPS[v].turnOn();
            
            bool ExitFlag = false;
            bool MuxFlag  = true;

            LOG(INFO) << "[RD53IVScan] Ready to run an IV curve from = " << vStart1 << " to " << vStop1 << " with steps of " << vStep << RESET;

            itIVtest.initITIVTools();
            
            if (MuxFlag == true)
            {
                std::string fileHeader = "currentD;currentA;InputA;ShuntA;InputD;ShuntD";
                itIVtest.prepareImuxFileHeader(fileHeader);
            }

            for(float vCurrent : FirstIVCurve)
            {
                progress.update( (vCurrent - vStart1) / (2*(vStop1 - vStart1)) );

                LOG(INFO) << "[RD53IVScan] Scanning current point " << vCurrent << " A " << RESET;
                std::string psRead           = "";
                std::string currentReadValue = "";
                for(unsigned int v = 0; v < channelsPS.size(); ++v) channelsPS[v].setCurrent(vCurrent);
                sleep(1);
                for(unsigned int v = 0; v < channelsPS.size(); ++v)
                {
                    std::string currentStr = std::to_string(channelsPS[v].getCurrent());
                    std::string voltageStr = std::to_string(channelsPS[v].getVoltage());
                    psRead                 = psRead + currentStr + ";" + voltageStr + ";";
                    currentReadValue       = currentReadValue + currentStr + ";";
                }
                if(MuxFlag == true)    
                {    for_each_device<Chip>(Base::system(),
                                        [&](Chip* chip)
                                        {
                                            for(unsigned int vIMUXcode = 28; vIMUXcode < 32; ++vIMUXcode)
                                            {
                                                try{
                                                    chipInterface.WriteReg(chip, "MonitorEnable", 1); // Choose MUX entry
                                                    chipInterface.WriteReg(chip, "VMonitor", 0b000001);
                                                    chipInterface.WriteReg(chip, "IMonitor", vIMUXcode);
                                                    chipInterface.SendGlobalPulse(chip, {"ADCStartOfConversion"}); // ADC start conversion
                                                    unsigned int iRead  = chipInterface.ReadReg(chip, "MonitoringDataADC", true);
                                                    std::string iMuxStr = std::to_string(iRead);
                                                    currentReadValue    = currentReadValue + iMuxStr + ";";
                                                }
                                                catch (const std::runtime_error& e) 
                                                {
                                                    LOG(ERROR) << BOLDRED << "Communication Error: current too low" << RESET;
                                                    ExitFlag=true;
                                                    break;
                                                }
                                                
                                            }
                                        });
                }

                scannerCardKeithley.readScannerCardPoint(psRead);
                currentReadValue.pop_back();
                
                if (ExitFlag == true)  break;
                else if (MuxFlag == true)    itIVtest.writeImuxLine(currentReadValue);
                sleep(1);
            }
            itIVtest.runAnalysis();
            
            LOG(INFO) << "[RD53IVScan] Ready to run an IV curve from = " << vStart2 << " to " << vStop2 << " with steps of " << vStep << RESET;
            
            itIVtest.initITIVTools();
            for(float vCurrent : SecondIVCurve)
            {
                progress.update( (vCurrent - vStart2) / (2*(vStop2 - vStart2)) + 0.5 );
                LOG(INFO) << "[RD53IVScan] Scanning current point " << vCurrent << " A " << RESET;
                std::string psRead           = "";
                std::string currentReadValue = "";
                for(unsigned int v = 0; v < channelsPS.size(); ++v) channelsPS[v].setCurrent(vCurrent);
                sleep(1);
                for(unsigned int v = 0; v < channelsPS.size(); ++v)
                {
                    std::string currentStr = std::to_string(channelsPS[v].getCurrent());
                    std::string voltageStr = std::to_string(channelsPS[v].getVoltage());
                    psRead                 = psRead + currentStr + ";" + voltageStr + ";";
                    currentReadValue       = currentReadValue + currentStr + ";";
                }
                scannerCardKeithley.readScannerCardPoint(psRead);
                currentReadValue.pop_back();
                sleep(1);
            }
            
            for(unsigned int v = 0; v < channelsPS.size(); ++v) channelsPS[v].setCurrent(finalCurrent);
            sleep(1);
            itIVtest.runAnalysis();
        }
        else
            LOG(ERROR) << BOLDRED
                       << "Bad "
                          "type"
                          " value in toml configuration file for IVScan, please choose among [complete steps]!"
                       << RESET;
        return results;
}

template class RD53IVScan<RD53BFlavor::ATLAS>;
template class RD53IVScan<RD53BFlavor::CMS>;

} // namespace RD53BTools

