#include "RD53BThresholdTuning.h"

namespace RD53BTools {

template <class Flavor>
void RD53BThresholdTuning<Flavor>::tune_regs(Task progress, const std::vector<typename RD53BConstants::Register>& regs, size_t minValue, size_t maxValue, double targetRatio, bool above, float occupancyThreshold) const {
    auto& chipInterface = Base::chipInterface();
    
    ChipDataMap<size_t> value;
    ChipDataMap<size_t> bestValue;
    ChipDataMap<double> bestRatio;
    ChipDataMap<pixel_matrix_t<Flavor, bool>> valid;

    ChipDataMap<pixel_matrix_t<Flavor, size_t>> stuckLimit;
    
    ChipDataMap<pixel_matrix_t<Flavor, bool>> originalEnable;
    ChipDataMap<pixel_matrix_t<Flavor, bool>> originalEnableInjections;

    size_t size = maxValue - minValue;
    size_t step = std::ceil(size / 4.0);
    size_t nSteps = std::ceil(std::log2(size));

    auto used = injectionTool->usedPixels();

    Base::for_each_chip([&] (auto* chip) {
        value[chip] = minValue + size / 2;
        bestValue[chip] = minValue;
        bestRatio[chip] = 100;
        valid[chip] = used && chip->injectablePixels();
        if (above)
            stuckLimit[chip].fill(minValue);
        else
            stuckLimit[chip].fill(maxValue);
        originalEnable[chip] = chip->pixelConfig().enable;
        originalEnableInjections[chip] = chip->pixelConfig().enableInjections;

        chipInterface.WriteReg(chip, Flavor::Reg::VCAL_MED, param("vcalMed"_s));
    });

    ChipDataMap<pixel_matrix_t<Flavor, bool>> assumedStuck;

    for (size_t i = 0; i < nSteps; ++i) {
        
        Base::for_each_chip([&] (auto* chip) {
            if (above) {
                assumedStuck[chip] = valid[chip] && value[chip] < stuckLimit[chip];
                chip->pixelConfig().enable = originalEnable[chip] && !assumedStuck[chip];
                chip->pixelConfig().enableInjections = originalEnableInjections[chip] && !assumedStuck[chip];
            }
            else {
                assumedStuck[chip] = valid[chip] && value[chip] > stuckLimit[chip];
                chip->pixelConfig().enable = originalEnable[chip] && !assumedStuck[chip];
                chip->pixelConfig().enableInjections = originalEnableInjections[chip] && !assumedStuck[chip];
            }

            for (const auto& reg : regs) {
                chipInterface.WriteReg(chip, reg, value[chip]);
                std::cout << reg.name << std::endl;
            }
            std::cout << value[chip] << std::endl;
            chipInterface.WriteReg(chip, Flavor::Reg::VCAL_HIGH, 0xFFF);
        });

        auto resultHighCharge = injectionTool->run(progress.subTask({i / double(nSteps), (i + .5) / double(nSteps)}));
        auto occMapHighCharge = injectionTool->occupancy(resultHighCharge);

        ChipDataMap<pixel_matrix_t<Flavor, bool>> stuck;

        Base::for_each_chip([&] (auto* chip) {
            stuck[chip] = valid[chip] && (assumedStuck[chip] || (occMapHighCharge[chip] <= param("stuckPixelOccThreshold"_s)));
            chipInterface.WriteReg(chip, Flavor::Reg::VCAL_HIGH, param("vcalMed"_s) + param("targetThreshold"_s));
        });

        auto result = injectionTool->run(progress.subTask({(i + .5) / double(nSteps), (i + 1) / double(nSteps)}));
        auto occMap = injectionTool->occupancy(result);

        Base::for_each_chip([&] (auto* chip) {
            // std::cout << "stuck: " << xt::count_nonzero(stuck[chip]) << std::endl;
            // std::cout << "valid: " << xt::count_nonzero(valid[chip]) << std::endl;
            std::cout << "stuck: " << xt::count_nonzero(valid[chip] && stuck[chip]) << std::endl;
            std::cout << "aboveTargetThreshold: " << xt::count_nonzero(valid[chip] && !stuck[chip] && occMap[chip] < occupancyThreshold) << std::endl;
            std::cout << "belowTargetThreshold: " << xt::count_nonzero(valid[chip] && !stuck[chip] && occMap[chip] > occupancyThreshold) << std::endl;
            std::cout << "belowTargetThresholdOrStuck: " << xt::count_nonzero(valid[chip] && (stuck[chip] || (occMap[chip] > occupancyThreshold))) << std::endl;
            double ratio;
            if (above) {
                ratio = 1 - xt::count_nonzero(valid[chip] && !stuck[chip] && (occMap[chip] < occupancyThreshold))() / (double)xt::count_nonzero(valid[chip])();
                xt::filtration(stuckLimit[chip], stuck[chip] && value[chip] < stuckLimit[chip]) = value[chip];
            }
            else {
                ratio = 1 - xt::count_nonzero(valid[chip] && (stuck[chip] || (occMap[chip] > occupancyThreshold)))() / (double)xt::count_nonzero(valid[chip])();
                xt::filtration(stuckLimit[chip], stuck[chip] && value[chip] > stuckLimit[chip]) = value[chip];
            }

            std::cout << "ratio: " << ratio << std::endl;
            
            if (ratio <= targetRatio && std::abs(ratio - targetRatio) < std::abs(bestRatio[chip] - targetRatio) + 1e-10) {
                bestValue[chip] = value[chip];
                bestRatio[chip] = ratio;
            }
            if (ratio > targetRatio)
                value[chip] += step;
            else 
                value[chip] -= step;
        });

        step = std::ceil(step / 2.0);
    }

    Base::for_each_chip([&] (auto* chip) {
        for (const auto& reg : regs)
            chipInterface.ConfigureReg(chip, reg, bestValue[chip]);
        chip->pixelConfig().enable = originalEnable[chip];
        chip->pixelConfig().enableInjections = originalEnableInjections[chip];
    });

    for (const auto& item : bestValue)
        LOG(INFO) << "Best value is " << item.second << " for chip: " << item.first << RESET;

    for (const auto& item : bestRatio)
        LOG(INFO) << "bestRatio is " << item.second << " for chip: " << item.first << RESET;
}

template <class Flavor>
void RD53BThresholdTuning<Flavor>::init() {
    param("thresholdEqualization"_s).param("targetThreshold"_s) = param("targetThreshold"_s);
    injectionTool = &(param("thresholdEqualization"_s).param("injectionTool"_s));
}

template <class Flavor>
tool_result_t<RD53BThresholdEqualization<Flavor>> RD53BThresholdTuning<Flavor>::run(Task progress) {
    auto& chipInterface = Base::chipInterface();

    Base::for_each_chip([&] (auto* chip) {
        chipInterface.UpdatePixelTDACUniform(chip, 31);
        chipInterface.WriteReg(chip, Flavor::Reg::DAC_LDAC_LIN, 0);
    });

    tune_regs(
        progress.subTask({0, 0.2}),
        {Flavor::Reg::DAC_GDAC_L_LIN, Flavor::Reg::DAC_GDAC_R_LIN, Flavor::Reg::DAC_GDAC_M_LIN},
        param("gdacRange"_s)[0],
        param("gdacRange"_s)[1],
        param("belowThresholdPixelRatio"_s),
        true,
        param("occupancyThresholdAbove"_s)
    );

    tune_regs(
        progress.subTask({0.2, 0.4}),
        {Flavor::Reg::DAC_LDAC_LIN},
        param("ldacRange"_s)[0],
        param("ldacRange"_s)[1],
        param("aboveThresholdPixelRatio"_s),
        false,
        param("occupancyThresholdBelow"_s)
    );

    return param("thresholdEqualization"_s).run(progress.subTask({0.4, 1}));
}

template <class Flavor>
void RD53BThresholdTuning<Flavor>::draw(const tool_result_t<RD53BThresholdEqualization<Flavor>>& result) {
    param("thresholdEqualization"_s).Draw(result);
}

template class RD53BThresholdTuning<RD53BFlavor::ATLAS>;
template class RD53BThresholdTuning<RD53BFlavor::CMS>;

} // namespace RD53BTools
