
import scan_routine_config

from datetime import datetime, timedelta
import xml.etree.ElementTree as ET
import toml
import sys, os, shutil, copy, time, csv, subprocess, itertools, argparse, importlib

powerSupplyResource = "TTi"
powerSupplyVoltage = 2.4
powerSupplyCurrent = 1.0

powerSupply=None

defaultConfigFile = "CROC.xml"

timeout = 600
maxAttempts = 3

runID = -1
baseDir = 'Runs' #/Run_' + str(runID) if runID >= 0 else datetime.now().strftime("%Y_%m_%d_%H_%M_%S")

doseRate = 2.46 #Mrad / hour

fmt = "%y_%m_%d-%H_%M_%S_%f"

def dose2timedelta(dose):
    return timedelta(hours=(dose / doseRate))

class TaskLog:
    fieldNames = ['name', 'start_time', 'end_time', 'status', 'failed_attempts', 'output_dir']

    def __init__(self, baseDir):
        self.filePath = os.path.join(baseDir, "log.csv")
        if not os.path.exists(self.filePath):
            with open(self.filePath, 'w', newline='') as f:
                csv.DictWriter(f, self.fieldNames).writeheader()
            self.rows = []
        else:
            with open(self.filePath, newline='') as f:
                self.rows = list(csv.DictReader(f))
    
    def add_row(self, *args):
        row = {self.fieldNames[i] : args[i] for i in range(len(self.fieldNames))}
        self.rows.append(row)
        with open(self.filePath, 'a', newline='') as f:
            csv.DictWriter(f, self.fieldNames).writerow(row)
    

def getTomlFile(configFile):
    tree = ET.parse(configFile)
    root = tree.getroot()
    return next(root.iter("CROC")).attrib["configfile"]


def run_Ph2_ACF(configFile, tools, updateConfig, outputDir):
    for i in range(maxAttempts):
        if i > 1 and powerSupply is not None:
            powerSupply.power_cycle()
            time.sleep(.5)
        extra_flags = ["-s"] if updateConfig else []
        cmd = [
            "RD53BminiDAQ", 
            "-f", configFile, 
            "-t", "RD53BTools.toml",
            "-h",
            "-o", outputDir, 
            *extra_flags, 
            *tools
        ]
        p = subprocess.Popen(cmd, cwd=baseDir)
        try:
            returncode = p.wait(timeout=timeout)
            if returncode == 0:
                return i
        except:
            p.terminate()
    return maxAttempts


def run_task(task, log):
    start_time = datetime.now()

    configFile = task.get('configFile', defaultConfigFile)
    tomlFile = os.path.join(baseDir, getTomlFile(configFile))
    updateConfig = task.get('updateConfig', False)
    outputDir = os.path.join("Results", task["name"] + "_" + datetime.now().strftime(fmt))
    
    if task.get("powerCycle", False):
        powerSupply.power_cycle()
        time.sleep(.5)

    original_values = []
    if "params" in task:
        tomlData = toml.load(tomlFile)
        for p in task['params']:
            if not updateConfig:
                original_values.append({key : tomlData[p["table"]].get(key, None) for key in p["keys"]})
            for key in p["keys"]:
                tomlData[p["table"]][key] = p["value"]
        with open(tomlFile, "w") as f:
                toml.dump(tomlData, f)

    n_attempts = run_Ph2_ACF(configFile, task['tools'], updateConfig, outputDir)
    
    log.add_row(
        task['name'], 
        start_time.strftime(fmt),
        datetime.now().strftime(fmt),
        'success' if n_attempts < maxAttempts else 'fail',
        n_attempts,
        outputDir
    )

    # restore original values
    if "params" in task and not updateConfig:    
        tomlData = toml.load(tomlFile)
        for i in range(len(original_values)):
            p = task['params'][i]
            for key in p['keys']:
                tomlData[p['table']][key] = original_values[i][key]
        with open(tomlFile, "w") as f:
                toml.dump(tomlData, f)

            
def run_tasks(tasks):
    if not os.path.exists(baseDir):
        os.makedirs(baseDir)

    log = TaskLog(baseDir)

    start_time = datetime.strptime(log.rows[0]["start_time"], fmt) if len(log.rows) else datetime.now()

    while len(tasks) > 0:
        now = datetime.now()
        elapsed = now - start_time

        maxDelay = timedelta(0)
        currentTask = None

        tasksToBeRemoved = []

        for task in tasks:
            doseIterator = copy.deepcopy(task.get('doseIterator', iter([0])))
            currentDose = None
            for currentDose in itertools.takewhile(lambda d: dose2timedelta(d) < elapsed, doseIterator):
                pass            
            nextDose = next(doseIterator, None)
            if currentDose is not None:
                last_time = next((datetime.strptime(row["start_time"], fmt) for row in reversed(log.rows) if row["name"] == task["name"]), None)
                scheduled_time = start_time + dose2timedelta(currentDose)
                if last_time is None or last_time < scheduled_time:
                    delay = now - scheduled_time
                    if delay > maxDelay:
                        currentTask = task
                        maxDelay = delay
                elif nextDose is None:
                    tasksToBeRemoved.append(task)

          
        for task in tasksToBeRemoved:
            print(f"removing task: {task['name']}")
            tasks.remove(task)

        if currentTask is not None:
            run_task(currentTask, log)
        else:
            time.sleep(.5)
    print("No tasks left")

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('--no-instruments', action='store_true', help='Flag to not use the instrument control library')
    parser.add_argument('-t', '--task-list', action='store', help='The python script containing the task list')
    parser.add_argument('-r', '--run-id', action='store', default=None, help='The run ID')
    parser.add_argument('-c', '--config-dir', action='store', default='.', help='The directory containing all the configuration files')
    args = parser.parse_args()

    global powerSupply
    global baseDir

    runID = args.run_id or datetime.now().strftime(fmt)

    baseDir = os.path.join(baseDir, 'Run_' + runID)

    print(baseDir)

    if not os.path.exists(baseDir):
        os.makedirs(baseDir)

    if args.no_instruments:
        powerSupply = None
    else:
        from instrument_control import PowerSupplyController
        powerSupply = PowerSupplyController(powerSupplyResource, 2)

    if powerSupply is not None:
        powerSupply.power_off()
        time.sleep(.5)
        # set power supply voltage/current
        powerSupply.set_voltage(0, powerSupplyVoltage)
        powerSupply.set_voltage(1, powerSupplyVoltage)
        powerSupply.set_current(0, powerSupplyCurrent)
        powerSupply.set_current(1, powerSupplyCurrent)
        powerSupply.power_on()

    # copy config files into baseDir
    for fileName in os.listdir(args.config_dir):
        if fileName.endswith(('.xml', '.csv', '.toml')):
            if not os.path.exists(os.path.join(baseDir, fileName)):
                shutil.copy(os.path.join(args.config_dir, fileName), baseDir)

    mod = importlib.import_module(args.task_list)
    
    tasks = []

    for task in mod.tasks:
        if 'doseIterator' not in task:
            task['doseIterator'] = iter([0])
        if 'params' in task:
            params = task['params']
            for indices in itertools.product(*[range(len(p['values'])) for p in params]):
                subtask = copy.deepcopy(task)
                for i in range(len(params)):
                    del subtask['params'][i]['values']
                    subtask['params'][i]['value'] = params[i]['values'][indices[i]]
                subtask['name'] += "_" + "_".join([str(i) for i in indices])
                tasks.append(subtask)
        else:
            tasks.append(task)

    run_tasks(tasks)

    if powerSupply is not None:
        powerSupply.power_off()


if __name__=='__main__':
    main()
