#ifndef RD53MuxScan_H
#define RD53MuxScan_H

#include "RD53BTool.h"

namespace RD53BTools {

template <class Flavor>
struct RD53MuxScan : public RD53BTool<RD53MuxScan, Flavor> {
    using Base = RD53BTool<RD53MuxScan, Flavor>;
    using Base::Base;

    struct Results {
        std::map<std::string, std::pair<double, double>> VMuxVolt;
        std::map<std::string, std::pair<double, double>> IMuxVolt;
    };

    Results run() const;

    void draw(const Results& results) const;
};

}

#endif



