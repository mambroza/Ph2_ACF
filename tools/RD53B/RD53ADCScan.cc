#include "RD53ADCScan.h"

#include "../ProductionTools/ITchipTestingInterface.h"

#include <TGraph.h>
#include <TStyle.h>
#include <TFitResult.h>
#include <TVector.h>


#define LOGNAME_FORMAT "%d%m%y_%H%M%S"
#define LOGNAME_SIZE 50


namespace RD53BTools {

template <class Flavor>
constexpr const char* RD53ADCScan<Flavor>::writeVar[];

template <class Flavor>
typename RD53ADCScan<Flavor>::Results RD53ADCScan<Flavor>::run() const {
    Results results;
    auto& chipInterface = Base::chipInterface();

    Ph2_ITchipTesting::ITpowerSupplyChannelInterface dKeithley2410(Base::system().fPowerSupplyClient, "TestKeithley", "Front");

    dKeithley2410.setupKeithley2410ChannelSense(VOLTAGESENSE, 2.0);

    auto chip = Base::firstChip();

    auto& ADCcode = results.ADCcode;
    auto& VMUXvolt = results.VMUXvolt;
    auto& fitStart = results.fitStart;
    auto& fitEnd = results.fitEnd;
    int stepSize = 100;
    

    for(int variable = 0; variable < param("nVariables"_s); variable++) {
        fitStart[variable] = 0;
        fitEnd[variable] = 0;
        ADCcode.push_back(std::vector<double>());
        VMUXvolt.push_back(std::vector<double>());
    }
    
    for(int input = 0; input < 4096; input+=stepSize)
    {
        if(input > 4096) continue;
        LOG(INFO) << BOLDBLUE << "i        = " << BOLDYELLOW << input << " " << RESET;
        
        for(int variable = 0; variable < param("nVariables"_s); variable++)
        {
            
            chipInterface.WriteReg(chip, "MEAS_CAP", 1);
            chipInterface.WriteReg(chip, writeVar[variable], input);
            chipInterface.SendGlobalPulse(chip, {"ResetADC"},1); //Reset ADC
            chipInterface.WriteReg(chip, "MonitorEnable", 1); //Choose MUX entry
            chipInterface.WriteReg(chip, "VMonitor", 0b000111);
            chipInterface.SendGlobalPulse(chip, {"ADCStartOfConversion"}); //ADC start conversion
            
            ADCcode[variable].push_back(chipInterface.ReadReg(chip, "MonitoringDataADC")); //Read ADC code
            VMUXvolt[variable].push_back(dKeithley2410.getVoltage());

            if(input > 1)
            {
                if(((ADCcode[variable].end()[-1] > 0 && ADCcode[variable].end()[-2] == 0) || (ADCcode[variable].end()[-2] > 0 && ADCcode[variable].end()[-1] == 0)) && fitStart[variable] == 0)
                    fitStart[variable] = ADCcode[variable].end()[-1]; 
                if(((ADCcode[variable].end()[-1] == 4095 && ADCcode[variable].end()[-2] < 4095) || (ADCcode[variable].end()[-1] < 4095 && ADCcode[variable].end()[-2] == 4095)) && fitEnd[variable] == 0)
                    fitEnd[variable] = ADCcode[variable].end()[-1];
                if(fitEnd[variable] == 0 && input >= 4000) fitEnd[variable] = ADCcode[variable].end()[-1];
            }
        }
    }

    return results;
}

template <class Flavor>
void RD53ADCScan<Flavor>::draw(const Results& results) {
    auto& ADCcode = results.ADCcode;
    auto& VMUXvolt = results.VMUXvolt;
    auto& fitStart = results.fitStart;
    auto& fitEnd = results.fitEnd;

    static char auxvar[LOGNAME_SIZE];
	time_t now = time(0);
	strftime(auxvar, sizeof(auxvar), LOGNAME_FORMAT, localtime(&now));
	std::string outputname;
	outputname = auxvar;	
	
    auto canvas = new TCanvas();
    TFile* file = new TFile((Base::getOutputBasePath() + "/ADC_linearity_" + outputname + ".root").c_str(), "new");
    canvas->Print((Base::getOutputBasePath() + "/ADCplots_" + outputname + ".pdf[").c_str());
	for(int variable = 0; variable < param("nVariables"_s); variable++)
	{
		TGraph* linear = new TGraph(ADCcode[variable].size(), &(ADCcode[variable][0]), &(VMUXvolt[variable][0]));
		linear->SetTitle("Linear Graph;ADC Code;Voltage");
        std::cout << "Fitting: " << writeVar[variable] << ", fitStart = " << fitStart[variable] << ", fitEnd = " << fitEnd[variable] << std::endl;
		linear->SetName(writeVar[variable]);
		linear->Fit("pol1", "", "", fitStart[variable], fitEnd[variable]);
		TFitResultPtr r = linear->Fit("pol1", "S", "", fitStart[variable], fitEnd[variable]);
		gStyle->SetOptFit(0);
		gStyle->SetFitFormat(".2g");
		linear->Draw("APL");
		canvas->Print((Base::getOutputBasePath() + "/ADCplots_" + outputname + ".pdf").c_str());
		linear->Write();
		//Save log of the ADC calibration
		static const std::string csvFileName = Base::getOutputBasePath() + "/ADCCalibration.csv";
		std::ofstream csvOutFile;
		if (boost::filesystem::exists(csvFileName))
			csvOutFile.open(csvFileName, std::ios_base::app);
		else {
			csvOutFile.open(csvFileName);
			csvOutFile << "time, Intercept, Slope\n";
		}
		auto now = time(0);
		csvOutFile << std::put_time(std::localtime(&now), "%Y-%m-%d %H:%M:%S, ");
		csvOutFile << r->Value(0) << ", " << r->Value(1) << "\n";
		//Save latest calibration for general use
		static const std::string txtFileName = Base::getOutputBasePath() + "/ADCCalibration.txt";
		std::ofstream txtOutFile;
		txtOutFile.open(txtFileName);
		txtOutFile << r->Value(0) << "\n";
		txtOutFile << r->Value(1) << "\n";
	}
    file->Write();
    canvas->Print((Base::getOutputBasePath() + "/ADCplots_" + outputname + ".pdf]").c_str());
    file->Close();


    // Base::createRootFile();

	// std::ofstream outFile(Base::getOutputFilePath("ADCCalibration.csv"));
	// outFile << "Intercept, Slope\n";
	
    // auto canvas = new TCanvas();

    // for(int variable = 0; variable < 1; variable++)
    // {
    //     TGraph* linear = new TGraph(results.ADCcode.size(), &(results.ADCcode[0][variable]), &(results.VMUXvolt[0][variable]));
    //     linear->SetTitle("Linear Graph;ADC Code;Voltage");
    //     linear->SetName(writeVar[variable]);
    //     linear->Fit("pol1", "", "", results.fitStart[variable], results.fitEnd[variable]);
    //     TFitResultPtr r = linear->Fit("pol1", "S", "", results.fitStart[variable], results.fitEnd[variable]);
    //     gStyle->SetOptFit(0);
    //     gStyle->SetFitFormat(".2g");
    //     linear->Draw("APL");
        
    //     outFile << r->Value(0) << ", " << r->Value(1) << "\n";

    //     linear->Write();
    // }

    // canvas->Print(Base::getOutputFilePath("ADC_plot.pdf").c_str());
}

template class RD53ADCScan<RD53BFlavor::ATLAS>;
template class RD53ADCScan<RD53BFlavor::CMS>;

}


