from itertools import count, chain

# Dose iterator used by most tasks, generates the sequence:
# 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 20, 50, 100, 200, 300, ...
mainDoseIterator = chain(range(11), [20, 50], count(100, 100))

tasks = [
    {
        'name': 'VrefTrimming',
        'tools': ['VrefTrimming'],
        'updateConfig': True,
        'doseIterator': count(0, 1) # 0, 1, 2, 3, ...
    },
    {
        'name': 'InitialThresholdTuning',
        'tools': ['GlobalThresholdTuning3000', 'ThresholdEqualization3000', 'GlobalThresholdTuning1000', 'ThresholdEqualization1000', 'ThresholdScanSparseLow'],
        'updateConfig': True
    },
    {
        'name': 'ShortRingOsc_Mux',
        'tools': ['ShortRingOsc', 'MuxScan'],
        'doseIterator': count(0, 1) # 0, 1, 2, 3, ...
    },
    {
        'name': 'IVConfigured',
        'tools': ['IVScanConfigured'],
        'doseIterator': mainDoseIterator
    },
    {
        'name': 'IVDefault',
        'powerCycle' : True,
        'configFile': 'CROCDefault.xml',
        'tools': ['IVScanDefault'],
        'doseIterator': mainDoseIterator
    },
    {
        'name': 'ThresholdScan_existingTDACs',
        'tools': ['StuckPixelScan', 'NoiseScan', 'ThresholdScanSparseLow'],
        'params': [
            {
                'table' : 'Pixels',
                'keys' : ['tdac'],
                'values' : ['tdac1000_cold.csv']
            },
            {
                'table' : 'Pixels',
                'keys' : ['enable'],
                'values' : ['1']
            }
        ],
        'doseIterator': mainDoseIterator
    },
    {
        'name': 'ThresholdTuning',
        'tools': ['GlobalThresholdTuning1000', 'ThresholdEqualization1000', 'ThresholdScanSparseLow'],
        'updateConfig': True,
        'doseIterator': mainDoseIterator
    },
    {
        'name': 'MaskNoisy',
        'tools': ['StuckPixelscan', 'NoiseScan'],
        'updateConfig': True,
        'doseIterator': mainDoseIterator
    },
    {
        'name': 'AFEScans',
        'tools': ['DigitalScan', 'AnalogScan', 'NoiseScan'],
        'doseIterator': mainDoseIterator
    },
    {
        'name': 'ChipBottomScans',
        'tools': ['RingOsc', 'ADCScan', 'DACScan'],
        'doseIterator': mainDoseIterator
    },    
    {
        'name': 'TimeWalk',
        'tools': ['TimeWalk'],
        'doseIterator': mainDoseIterator
    },
    {
        'name': 'TimeWalkFine',
        'tools': ['TimeWalkFine'],
        'doseIterator': mainDoseIterator
    }
]
