#ifndef RD53BINJECTIONTOOL_H
#define RD53BINJECTIONTOOL_H

#include "RD53BTool.h"
#include "../Utils/RD53BEventDecoding.h"

class TH1;

namespace RD53BTools {

template <class>
struct RD53BInjectionTool; // forward declaration

using MaskStep = decltype(make_named_tuple(
    std::make_pair("dim"_s, size_t{}),
    std::make_pair("size"_s, size_t{}),
    std::make_pair("parallel"_s, bool{}),
    std::make_pair("shift"_s, std::vector<size_t>{})
));

template <class Flavor>
const auto ToolParameters<RD53BInjectionTool<Flavor>> = make_named_tuple(
    std::make_pair("nInjections"_s, 10ul),
    std::make_pair("triggerDuration"_s, 10ul),
    std::make_pair("triggerLatency"_s, 133ul),
    std::make_pair("injectionType"_s, std::string("Analog")),
    std::make_pair("readoutPeriod"_s, 0ul),
    std::make_pair("delayAfterPrime"_s, 100),
    std::make_pair("delayAfterInject"_s, 32),
    std::make_pair("delayAfterTrigger"_s, 800),
    std::make_pair("fineDelay"_s, 0ul),
    std::make_pair("pulseDuration"_s, 6ul),
    std::make_pair("offset"_s, std::vector<size_t>({0, 0})),
    std::make_pair("size"_s, std::vector<size_t>({0, 0})),
    std::make_pair("frameStep"_s, 1ul),
    std::make_pair("disableUnusedPixels"_s, true),
    std::make_pair("injectUnusedPixels"_s, false),
    std::make_pair("decoderThreads"_s, 1u),
    std::make_pair("enableToTReadout"_s, true),
    std::make_pair("enableChipIdReadout"_s, false),
    std::make_pair("enable"_s, 50u),
    std::make_pair("maskGen"_s, std::vector<MaskStep>({
        MaskStep(0, 0, 0, {8, 1}),
        MaskStep(1, 0, 1, {})
    }))
);

struct ScanRange {
    ScanRange() {}

    template <class F>
    ScanRange(size_t size, F&& f)
    : _size(size)
    , _f(std::forward<F>(f))
    {}

    size_t size() const { return _size; }
    
    void apply(size_t i) { _f(i); }

private:
    size_t _size;
    std::function<void(int)> _f;
};

template <class... Ts>
auto makeScan(Ts&&... args) {
    return std::array<ScanRange, sizeof...(Ts)>{std::forward<Ts>(args)...};
}

template <class Flavor>
struct RD53BInjectionTool : public RD53BTool<RD53BInjectionTool, Flavor> {
    using Base = RD53BTool<RD53BInjectionTool, Flavor>;
    using Base::Base;
    using Base::param;

    using ChipEventsMap = ChipDataMap<std::vector<RD53BEventDecoding::RD53BEvent>>;

    void init();

    ChipEventsMap run(Task progress) const;

    template <size_t N, class A>
    void injectionScan(Task progress, const std::array<ScanRange, N>& scanRanges, A&& analyzer) const;

    ChipDataMap<pixel_matrix_t<Flavor, double>> occupancy(const ChipEventsMap& result) const;

    ChipDataMap<std::array<size_t, 16>> totDistribution(const ChipEventsMap& result) const;

    size_t nFrames() const { return _nFrames; }

    size_t nEvents() const { return param("nInjections"_s) * param("triggerDuration"_s); }

    void draw(const ChipEventsMap& result);

    pixel_matrix_t<Flavor, bool> usedPixels() const;

    pixel_matrix_t<Flavor, bool> generateInjectionMask(size_t i) const;

private:
    void configureInjections() const;

    void setupMaskFrame(size_t frameId) const;

    template <size_t N>
    std::array<size_t, N> index2coords(size_t i, const std::array<size_t, N>& sizes) const;
    
    size_t _nFrames;
};


template <class Flavor>
template <size_t N>
std::array<size_t, N> RD53BInjectionTool<Flavor>::index2coords(size_t i, const std::array<size_t, N>& sizes) const {
    std::array<size_t, N> p{0};
    for (int dim = N - 1; dim >= 0; --dim) {
        p[dim] = i % sizes[dim];
        i = i / sizes[dim];
    }
    return p;
}


// General N-dimensional injection scan.
// The scan over the injection pattern is allways the first dimension.
template <class Flavor>
template <size_t N, class A>
void RD53BInjectionTool<Flavor>::injectionScan(Task progress, const std::array<ScanRange, N>& scanRanges, A&& analyzer) const {
    configureInjections();

    typename Flavor::FormatOptions options;
    options.enableToT = param("enableToTReadout"_s);
    options.enableChipId = param("enableChipIdReadout"_s);

    // AsyncEventDecoder is a thread pool that can decode events asynchronously
    RD53BEventDecoding::AsyncEventDecoder<Flavor> decoder{param("decoderThreads"_s), options};
    
    std::array<ScanRange, N + 1> ranges;
    ranges[0] = ScanRange(_nFrames, [&] (auto i) { setupMaskFrame(i); });
    std::copy(scanRanges.begin(), scanRanges.end(), ranges.begin() + 1);

    // the size of each dimension of the scan
    std::array<size_t, N + 1> scan_shape;
    std::transform(ranges.begin(), ranges.end(), scan_shape.begin(), [] (const auto& r) { return r.size(); });

    std::array<size_t, N + 1> result_shape;
    std::copy(scan_shape.begin() + 1, scan_shape.end(), result_shape.begin());
    result_shape[N] = nEvents();
    
    std::map<size_t, ChipDataMap<xt::xarray<RD53BEventDecoding::RD53BEvent>>> events;

    // get the total size of the scan
    size_t size = std::accumulate(scan_shape.begin(), scan_shape.end(), 1, std::multiplies<>());

    size_t pointsPerMask = size / _nFrames;

    // tracks how many points have been completed to update the progress bar
    size_t nDone = 0;

    // tracks which points are completed
    std::vector<bool> done(size, false);

    // tracks which frames of the injection pattern have been analyzed
    std::vector<bool> analyzed(_nFrames, false);

    // maps point ids to futures that can be used to get the corresponding events once decoded
    std::map<size_t, std::future<RD53BEventDecoding::DecodingResult>> decodingFutures;

    // main loop
    while (!std::all_of(analyzed.begin(), analyzed.end(), [] (auto x) { return x; })) {

        // current index for each dimension
        std::vector<size_t> indices(N + 1, 0);

        // the first dimension that was updated (in the first iteration all dimensions are updated)
        size_t minDim = 0;

        if (!std::all_of(done.begin(), done.end(), [] (auto x) { return x; })) {

            for (size_t i = 0; i < size; ++i) {
                
                if (!done[i] && decodingFutures.find(i) == decodingFutures.end()) {
                    for (size_t dim = minDim; dim < ranges.size(); ++dim)
                        ranges[dim].apply(indices[dim]);
                    minDim = ranges.size();

                    std::vector<uint32_t> data;
                    Base::for_each_board([&] (BeBoard* board) {
                        auto& fwInterface = Base::getFWInterface(board);
                        fwInterface.template GetEventData<Flavor>(board, data, Base::chipInterface());
                    });
                    
                    decodingFutures.insert({i, decoder.decode_events(std::move(data))});
                    
                    if (decodingFutures.size() > 5)
                        break;
                }

                // update indices and set minDim to the minimum dimension that was updated
                for (int dim = ranges.size() - 1; dim >= 0; --dim) {
                    if (indices[dim] < ranges[dim].size() - 1) {
                        ++indices[dim];
                        if ((int)minDim > dim)
                            minDim = dim;
                        break;
                    }
                    else
                        indices[dim] = 0;
                }
                
            }
            
            // process decoded events
            for (auto it = decodingFutures.begin(); it != decodingFutures.end();) {
                auto& future = it->second;
                if (future.wait_for(std::chrono::seconds(0)) == std::future_status::ready)
                {
                    auto result = future.get();

                    auto scan_coords = index2coords(it->first, scan_shape);

                    if (result) {
                        // group events by chip
                        ChipEventsMap events_local;
                        for (auto& event : result.value()) {
                            for (auto& chipEvent : event.events)
                                events_local[{chipEvent.hybridId, chipEvent.chipLane}].emplace_back(event.l1a_counter, std::move(chipEvent.hits));
                        }
                        // check if we have the right number of events for each chip
                        done[it->first] = true; // assume all ok
                        Base::for_each_chip([&] (auto chip) {
                            size_t nEventsLocal = events_local[chip].size();
                            if (nEventsLocal != nEvents()) {
                                LOG (WARNING) << "Got " << nEventsLocal << "/" << nEvents() << 
                                    " events for injection round #" << it->first << ". Repeating...";
                                done[it->first] = false; // not ok
                            }
                        });

                        if (done[it->first]) {
                            // move current batch of events into the overall event storage
                            
                            std::array<size_t, N> result_coords;
                            std::copy(scan_coords.begin() + 1, scan_coords.end(), result_coords.begin());
                            
                            auto it = events.find(scan_coords[0]);
                            if (it == events.end()) {
                                it = events.insert({scan_coords[0], {}}).first;
                                Base::for_each_chip([&] (auto chip) {
                                    it->second.insert({chip, xt::empty<RD53BEventDecoding::RD53BEvent>(result_shape)});
                                });
                            }
                            
                            Base::for_each_chip([&] (auto chip) {
                                // index_nested_vector(events[chip], scan_coords) = std::move(events_local);
                                xt::xstrided_slice_vector sv;
                                for (auto c : result_coords)
                                    sv.push_back(c);
                                auto slice = xt::strided_view(it->second[chip], sv);

                                std::move(
                                    events_local[chip].begin(),
                                    events_local[chip].end(),
                                    slice.begin()
                                );
                            });
                            progress.update(++nDone / (double)size);
                        }
                    }
                    else {
                        std::ostringstream oss;
                        oss << "Decoding error occured during injection (";
                        for (auto c : scan_coords)
                            oss << c << " ";
                        oss << "): " << result.error();
                        LOG(ERROR) << BOLDRED << oss.str() << RESET;
                    }

                    it = decodingFutures.erase(it);
                }
                else
                    ++it;
            }
        }

        // analyze events for pixels belonging to frames of the injection pattern that have been completed
        for (size_t i = 0; i < _nFrames; ++i) {
            if (!analyzed[i] && std::all_of(done.begin() + i * pointsPerMask, done.begin() + (i + 1) * pointsPerMask, [] (auto x) { return x; })) {
                std::forward<A>(analyzer)(i, std::move(events[i]));
                events.erase(i);
                analyzed[i] = true;
            }
        }
    }
    
    // reset masks
    Base::for_each_chip([&] (Chip* chip) {
        Base::chipInterface().UpdatePixelConfig(chip, true, false);
        // events[chip].resize(result_shape);
    });

    // return events;
}

}

#endif